# vue-mastery

Learn Vue via https://www.vuemastery.com/

## Beginner
- [ ] [Intro to Vue.js](https://www.vuemastery.com/courses/intro-to-vue-js/vue-instance/)
- [ ] [Animating Vue](https://www.vuemastery.com/courses/animating-vue/why-animate/)
- [ ] [Beautify with Vuetify](https://www.vuemastery.com/courses/beautify-with-vuetify/getting-started-with-vuetify/)

## Intermediate
- [ ] [Real World Vue.js](https://www.vuemastery.com/courses/real-world-vue-js/real-world-intro/)
- [ ] [Mastering Vuex](https://www.vuemastery.com/courses/mastering-vuex/intro-to-vuex/)
- [ ] [Next-Level Vue](https://www.vuemastery.com/courses/next-level-vue/next-level-vue-orientation/)
- [ ] [Unit Testing](https://www.vuemastery.com/courses/unit-testing/what-to-test)
- [ ] [Token-Based Authentication](https://www.vuemastery.com/courses/token-based-authentication/intro-to-authentication/)
- [ ] [Watch Us Build a Trello Clone](https://www.vuemastery.com/courses/watch-us-build-trello-clone/tour-of-the-app/)
- [ ] [Vue 3 Essentials](https://www.vuemastery.com/courses/vue-3-essentials/why-the-composition-api/)

## Advanced
- [ ] [Scaling Vue with Nuxt.js](https://www.vuemastery.com/courses/scaling-vue-with-nuxt-js/why-use-nuxt/)
- [ ] [Advanced Components](https://www.vuemastery.com/courses/advanced-components/the-introduction/)